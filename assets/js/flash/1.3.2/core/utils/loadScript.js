/**
 * Lazy load a script appending a script tag to the body
 * 
 * @param  {string}   url         the url of the script
 * @param  {Function} callback    the code to execute when the script is loaded
 *
 * example: flash.loadScript('https://cdn.jsdelivr.net/npm/vue/dist/vue.js', function(){alert('Vue.js loaded')});
 */
flashCore.prototype.loadScript = function(url, callback) {
    var self = this;

	// Checking if the script is already loaded in the current window
	if(typeof window.loadedScripts === 'undefined') {
		window.loadedScripts = [];
	}
	if(window.loadedScripts.indexOf(url) == -1) {
		// Preparing and appending the script tag
		var script 		= document.createElement('script');
		script.type 	= 'text/javascript';
		script.src 		= url;
		script.async 	= 'true';
		script.defer 	= 'true';
		document.body.appendChild(script);
		script.addEventListener('load', callback);
	} else callback();
	
	// Adding the script to the loaded ones array
	window.loadedScripts.push(url);
}